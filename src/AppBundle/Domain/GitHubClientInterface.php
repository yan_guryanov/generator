<?php

namespace AppBundle\Domain;

interface GitHubClientInterface
{
    /**
     * @param string $username
     *
     * @return array
     */
    public function getUserRepositories($username);

    /**
     * @param string $username
     * @param string $repoName
     *
     * @return array
     */
    public function getRepositoryLanguages($username, $repoName);

    /**
     * @param string $username
     *
     * @return array
     */
    public function getUser($username);
}