<?php

namespace AppBundle\Domain;

use AppBundle\Exception\RateLimitReachedException;
use AppBundle\Exception\UserNotFoundException;
use \GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\BadResponseException;
use Psr\Http\Message\ResponseInterface;

class GitHubClient implements GitHubClientInterface
{
    const START_PAGE        = 1;
    const COUNT_PER_PAGE    = 100;
    
    /**
     * @var GuzzleHttpClient
     */
    private $client;

    /**
     * @var string
     */
    private $token;

    /**
     * @param GuzzleHttpClient $client
     * @param string $token
     */
    public function __construct(GuzzleHttpClient $client, $token)
    {
        $this->client = $client;
        $this->token = $token;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserRepositories($username)
    {
        return $this->getDataList(sprintf('/users/%s/repos', $username));
    }

    /**
     * {@inheritdoc}
     */
    public function getRepositoryLanguages($username, $repoName)
    {
        return $this->getDataList(sprintf('/repos/%s/%s/languages', $username, $repoName));
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($username)
    {
        $response = $this->get(sprintf('/users/%s', $username));
        $userData = json_decode($response->getBody(), true);

        return $userData;
    }

    /**
     * @param string    $url
     *
     * @return array
     */
    private function getDataList($url)
    {
        $result = [];
        $page = self::START_PAGE;
        do {
            $response = $this->get($url, $page);
            $list = json_decode($response->getBody(), true);
            
            $count = count($list);
            $result = array_merge($result, $list);
            $page ++;
        } while ($count === self::COUNT_PER_PAGE);
        
        return $result;
    }

    /**
     * @param string    $url
     * @param int       $page
     * @param array     $queryParams
     *
     * @return ResponseInterface
     *
     * @throws UserNotFoundException
     * @throws RateLimitReachedException
     */
    private function get($url, $page = self::START_PAGE, array $queryParams = [])
    {
        try {
            $options = [
                'query' => array_merge_recursive($queryParams, [
                    'page'      => $page,
                    'per_page'  => self::COUNT_PER_PAGE,
                ]),
                'auth' => ['token', $this->token],
            ];

            return $this->client->get($url, $options);    
        } catch (BadResponseException $e) {
            switch ($e->getCode()) {
                case 403:
                    throw new RateLimitReachedException('Rate limit was reached');
                    
                case 404:
                    throw new UserNotFoundException('User with the such username was not found on Git Hub');
                    
                default:
                    throw $e;
            }
        }
    }
}