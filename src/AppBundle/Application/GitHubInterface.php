<?php

namespace AppBundle\Application;

use AppBundle\Entity\Account;
use AppBundle\Entity\GitHub\Account as GitHubAccount;

interface GitHubInterface
{
    /**
     * @param Account $account
     *
     * @return GitHubAccount
     */
    public function getGitHubAccount(Account $account);
}