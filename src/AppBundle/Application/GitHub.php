<?php

namespace AppBundle\Application;

use AppBundle\Domain\GitHubClientInterface;
use AppBundle\Entity\Account;
use AppBundle\Entity\GitHub\Language;
use AppBundle\Entity\GitHub\Repository;
use AppBundle\Entity\GitHub\Account as GitHubAccount;

class GitHub implements GitHubInterface
{
    /**
     * @var GitHubClientInterface
     */
    private $client;

    /**
     * @param GitHubClientInterface $client
     */
    public function __construct(GitHubClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritdoc}
     */
    public function getGitHubAccount(Account $account)
    {
        $repositoriesList = $this->client->getUserRepositories($account->getName());
        $repositories = [];
        foreach ($repositoriesList as $repository) {
            $repositoryLanguages = $this->client->getRepositoryLanguages($account->getName(), $repository['name']);
            $languages = [];
            foreach ($repositoryLanguages as $languageName => $languageSize) {
                $languages[] = new Language($languageName, $languageSize);
            }
            $repositories[] = new Repository(
                $repository['name'],
                $repository['html_url'],
                $repository['description'],
                $repository['language'],
                $languages
            );
        }
        
        $userData = $this->client->getUser($account->getName());
        
        return new GitHubAccount($userData['name'], $userData['blog'], $repositories);
    }
}