<?php

namespace AppBundle\Entity\GitHub;

class Repository
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Language|null
     */
    private $primaryLanguage;

    /**
     * @var Language[]
     */
    private $languages;

    /**
     * @var int
     */
    private $totalSize;

    /**
     * @param string        $name
     * @param string        $link
     * @param string        $description
     * @param string        $primaryLanguageName
     * @param Language[]    $languages
     */
    public function __construct($name, $link, $description, $primaryLanguageName, array $languages = [])
    {
        $this->name         = $name;
        $this->link         = $link;
        $this->description  = $description;

        $this->totalSize = 0;
        foreach ($languages as $language) {
            $this->totalSize += $language->getSize();
            if ($language->getName() === $primaryLanguageName) {
                $this->primaryLanguage = $language;
            }
        }
        
        foreach ($languages as $language) {
            $percent = round($language->getSize() / $this->totalSize * 100, 2);
            $language->setPercent($percent);
        }
        
        $this->languages = $languages;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return Language|null
     */
    public function getPrimaryLanguage()
    {
        return $this->primaryLanguage;
    }

    /**
     * @return Language[]
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @return int
     */
    public function getTotalSize()
    {
        return $this->totalSize;
    }
}
