<?php

namespace AppBundle\Entity\GitHub;

class Account
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $website;

    /**
     * @var Repository[]
     */
    private $repositories;

    /**
     * @var float[]
     */
    private $languagePercentages;

    /**
     * @param string       $username
     * @param string       $website
     * @param Repository[] $repositories
     */
    public function __construct($username, $website, array $repositories)
    {
        $this->username = $username;
        $this->website = $website;
        $this->repositories = $repositories;
        
        $languageSizes = [];
        $totalSize = 0;
        foreach ($repositories as $repository) {
            $totalSize += $repository->getTotalSize();
            if (!$repository->getPrimaryLanguage()) {
                continue;
            }
            
            $primaryLanguageName = $repository->getPrimaryLanguage()->getName();
            if (!isset($languageSizes[$primaryLanguageName])) {
                $languageSizes[$primaryLanguageName] = 0;
            }
            $languageSizes[$primaryLanguageName] += $repository->getPrimaryLanguage()->getSize();
        }

        $this->languagePercentages = [];
        foreach ($languageSizes as $language => $size) {
            $this->languagePercentages[$language] = round($size / $totalSize * 100, 2);       
        }
        
        arsort($this->languagePercentages);
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @return Repository[]
     */
    public function getRepositories()
    {
        return $this->repositories;
    }

    /**
     * @return float[]
     */
    public function getLanguagePercentages()
    {
        return $this->languagePercentages;
    }
}