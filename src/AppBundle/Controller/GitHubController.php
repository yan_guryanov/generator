<?php

namespace AppBundle\Controller;

use AppBundle\Application\GitHubInterface;
use AppBundle\Exception\RateLimitReachedException;
use AppBundle\Form\AccountType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class GitHubController extends Controller
{
    /**
     * @Route(path="/", name="github", methods={"GET", "POST"})
     *
     * @param Request $request 
     *
     * @return Response
     */
    public function getMainFormAction(Request $request)
    {
        $form = $this->createForm(AccountType::class);
        $form->handleRequest($request);
        
        $limitReached = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $account = $form->getData();
            
            try {
                $gitHubAccount = $this->getGitHubApp()->getGitHubAccount($account);
                
                return $this->render('@App/GitHub/resume.html.twig', [
                    'account'    => $gitHubAccount,
                ]);
            } catch (RateLimitReachedException $e) {
                $form->addError(new FormError($e->getMessage()));
            } catch (\Exception $e) {
                $form->addError(new FormError('Internal Server Error'));
            }
        }

        return $this->render('@App/GitHub/form.html.twig', [
            'formView'      => $form->createView(),
            'limitReached'  => $limitReached,
        ]);
    }

    /**
     * @return GitHubInterface
     */
    private function getGitHubApp()
    {
        return $this->get('app.application.github');
    }
}
