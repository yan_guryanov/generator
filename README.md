1. Clone git repository into directory
2. Copy ```app/config/parameter.yml.dist``` to ```app/config/parameter.yml```
3. Change parameters in ```app/config/parameter.yml``` to yours.
4. Run ```php composer.phar install```
5. Run ```php bin/console server:run``` or ```php bin/console server:start``` or setup nginx/apache
